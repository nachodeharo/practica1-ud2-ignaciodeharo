package com.ignacioWolf.gymbbdd.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Vista {
    private JPanel panel1;
    protected JFrame frame;
    protected JTextField txtNombre;
    protected JTextField txtApellidos;
    protected JTextField txtDni;
    protected JTextField txtAltura;
    protected JTextField txtCodigo;
    protected JButton buscarButton;
    protected JButton nuevoButton;
    protected JButton eliminarButton;
    protected JButton verInstructoresButton;
    protected JTable tablaCliente;
    protected JTable tablaInstructor;
    protected DateTimePicker dateTimePicker;
    protected JRadioButton instructorRadioButton;
    protected JRadioButton clienteRadioButton;
    protected JLabel lblAction;

    protected DefaultTableModel dtm;
    protected DefaultTableModel dtm1;
    protected JMenuItem itemConectar;
    protected JMenuItem itemCrearTabla;
    protected JMenuItem itemCrearTabla2;
    protected JMenuItem itemSalir;
    protected JMenuItem itemUser;

    public Vista() {
        frame = new JFrame("Gimnasio");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        dtm =new DefaultTableModel();
        tablaCliente.setModel(dtm);

        dtm1 =new DefaultTableModel();
        tablaInstructor.setModel(dtm1);

        crearMenu();

        frame.pack();
        frame.setVisible(true);

    }
    private void crearMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");

        itemCrearTabla = new JMenuItem("Crear tabla Instructor");
        itemCrearTabla.setActionCommand("CrearTablaInstructor");

        itemCrearTabla2 = new JMenuItem("Crear tabla Cliente");
        itemCrearTabla2.setActionCommand("CrearTablaCliente");

        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        itemUser = new JMenuItem("Usuario/Contrasenya");
        itemUser.setActionCommand("UserPass");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemCrearTabla);
        menuArchivo.add(itemCrearTabla2);
        menuArchivo.add(itemSalir);
        menuArchivo.add(itemUser);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);
    }
}

