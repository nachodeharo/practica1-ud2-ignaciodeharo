package com.ignacioWolf.gymbbdd.gui;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.Properties;


/**
 * Clase Modelo que pemite el acceso a la base de datos y crear dichas tablas y registrar datos
 */
public class Modelo {
    private Connection conexion;
    protected String user;
    protected String pass;
    Properties configuracion;

    /**
     * Constructor de la clase Modelo, que permite inicializar la configuración
     * @throws IOException
     */
    public Modelo() throws IOException {
        configuracion = new Properties();
        configuracion.load(new FileReader("configuracion.conf"));
        user = configuracion.getProperty("User");
        pass = configuracion.getProperty("Pass");
    }

    /**
     * Nueva configuración que establece nuevo user y nueva pass
     * @param user
     * @param pass
     */
    public void nuevaConf(String user, String pass){
        this.user = user;
        this.pass = pass;

        System.out.println("Nuevo user: " + this.user + ", \n Nueva pass: " + this.pass);
    }

    /**
     * Crea nueva tabla de Instructores si no existiera
     * @throws SQLException
     */
    public void crearTablaInstructores() throws SQLException {
        conexion=null;
        conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/gimnasio",user,pass);

        String sentenciaSql="call crearTablaInstructores()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * Crea nueva tabla de Clientes si no existiera
     * @throws SQLException
     */
    public void crearTablaClientes() throws SQLException {
        conexion=null;
        conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/gimnasio","root","");

        String sentenciaSql="call crearTablaClientes()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * Conecta a la base de datos correspondiente
     * @throws SQLException
     */
    public void conectar() throws SQLException {
        conexion=null;
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/gimnasio", "root", "");
        }catch (SQLException ex){
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/", "root", "");
            String consulta ="CREATE DATABASE gimnasio";
            PreparedStatement sentencia = null;
            sentencia=conexion.prepareStatement(consulta);
            sentencia.executeUpdate();
        }
    }

    /**
     * Desconecta a la base de datos correspondiente
     * @throws SQLException
     */
    public void desconectar() throws SQLException {
        conexion.close();
        conexion=null;
    }

    /**
     * Saca todos los datos de la clase clientes
     * @return ResultSet de toda la BBDD
     * @throws SQLException
     */
    public ResultSet sacarDatos() throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta ="SELECT * FROM clientes";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Saca los codigos de los instructores y cuantos clientes tienen
     * @return ResultSet de la cantidad de codigos utilizados
     * @throws SQLException
     */
    public ResultSet sacarDatosCodigo() throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta ="SELECT codigoInstructor, COUNT(*) FROM clientes GROUP BY codigoInstructor";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }


    /**
     * Introduce un cliente en la bbdd
     * @param nombre nombre del cliente
     * @param apellidos apellidos del cliente
     * @param dni dni del cliente
     * @param altura altura del cliente
     * @param codigoInstructor codigo del cliente (de un instructor)
     * @param fechaMatriculacion fecha de alta
     * @return numero int (-1) de si da error en la bbdd
     * @throws SQLException
     */
    public int nuevoCliente(String nombre, String apellidos, String dni, int altura, String codigoInstructor,
                            LocalDateTime fechaMatriculacion) throws SQLException {
        if (conexion==null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta="INSERT INTO clientes(nombre, apellidos, dni, altura, codigoInstructor, fecha_matriculacion)"+
                "VALUES (?,?,?,?,?,?)";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,nombre);
        sentencia.setString(2,apellidos);
        sentencia.setString(3,dni);
        sentencia.setInt(4,altura);
        sentencia.setString(5,codigoInstructor);
        sentencia.setTimestamp(6,Timestamp.valueOf(fechaMatriculacion));

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return numeroRegistros;
    }


    /**
     * Introduce un instructor en la bbdd
     * @param nombre nombre del instructor
     * @param apellidos apellidos del instructor
     * @param dni dni del instructor
     * @param altura altura del instructor
     * @param numeroIdentificador codigo del instructor
     * @param fechaMatriculacion fecha de alta
     * @return numero int (-1) de si da error en la bbdd
     * @throws SQLException
     */
    public int nuevoInstructor(String nombre, String apellidos, String dni, int altura, String numeroIdentificador,
                            LocalDateTime fechaMatriculacion) throws SQLException {
        if (conexion==null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta="INSERT INTO instructor(nombre, apellidos, dni, altura, numeroIdentificador, fecha_matriculacion)"+
                "VALUES (?,?,?,?,?,?)";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,nombre);
        sentencia.setString(2,apellidos);
        sentencia.setString(3,dni);
        sentencia.setInt(4,altura);
        sentencia.setString(5,numeroIdentificador);
        sentencia.setTimestamp(6,Timestamp.valueOf(fechaMatriculacion));

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return numeroRegistros;
    }

    /**
     * Borra el cliente
     * @param id de la fila en la base de datos (y en la interfaz)
     * @return int si diese error en la bbdd
     * @throws SQLException
     */
    public int borrarCliente(int id) throws SQLException {
        if (conexion==null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta="DELETE FROM clientes WHERE id=?";
        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);
        sentencia.setInt(1,id);

        int resultado=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return resultado;

    }


    /**
     * Modifica un cliente
     * @param id del cliente
     * @param nombre del cliente
     * @param apellidos del cliente
     * @param dni del cliente
     * @param altura del cliente
     * @param codigoInstructor de un instructor asociado
     * @param fechaMatriculacion del cliente
     * @return int si diera un número
     * @throws SQLException
     */
    public int modificarCliente (int id, String nombre, String apellidos, String dni, int altura, String codigoInstructor,
                                 Timestamp fechaMatriculacion) throws SQLException {
        if (conexion==null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta="UPDATE clientes SET nombre=?, apellidos=?,"+
                "dni=?,altura=?, codigoInstructor=?, fecha_matriculacion=? WHERE id=?";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,nombre);
        sentencia.setString(2,apellidos);
        sentencia.setString(3,dni);
        sentencia.setInt(4,altura);
        sentencia.setString(5,codigoInstructor);
        sentencia.setTimestamp(6,fechaMatriculacion);
        sentencia.setInt(7,id);

        int resultado=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return resultado;


    }


    /**
     * Comprueba el código de un instructor y devuelve true o false
     * @param numeroIdentificador numero a comparar en la bbdd
     * @return true o false si existe o no el codigo
     * @throws SQLException
     */
    public boolean comprobarCodigo(String numeroIdentificador) throws SQLException {
        if (conexion==null) {
            return false;
        }
        if (conexion.isClosed()) {
            return false;
        }
        String consulta ="SELECT numeroIdentificador FROM instructor WHERE numeroIdentificador = ?";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        sentencia.setString(1, numeroIdentificador);
        ResultSet resultado=sentencia.executeQuery();

        if (resultado==null || resultado.next() == false){
            return false;
        }else{
            return true;
        }
    }


    /**
     * Busca el Cliente segun un dni
     * @param dni del cliente
     * @return fila del cliente seleccionado según DNI
     * @throws SQLException
     */
    public ResultSet buscarCliente(String dni) throws SQLException {
        //SIN HACER
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta ="SELECT * FROM clientes WHERE dni = ?";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        sentencia.setString(1, dni);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Permite establecer y rellenar la segunda tabla del código
     * @return Filas de cuantos clientes/numeros tiene la bbdd
     * @throws SQLException
     */
    public ResultSet clientesSegunCodigo() throws SQLException {
        System.out.println("LLEGANDO");
        String sentenciaSql="call clientesSegunCodigo()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        ResultSet resultado = procedimiento.executeQuery();

        return resultado;
    }
}
