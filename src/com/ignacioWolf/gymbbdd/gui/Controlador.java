package com.ignacioWolf.gymbbdd.gui;

import com.ignacioWolf.gymbbdd.principal.Util;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Clase Controlador que implementa ActionListener (Para los botones), TableModelListener (Para ver la tabla) y el
 * WindowListener (Para hacer un mensaje de despedida)
 */
public class Controlador implements ActionListener, TableModelListener, WindowListener {
    private Vista vista;
    private Modelo modelo;
    private String pass;


    private enum tipoEstado {conectado, desconectado};
    private tipoEstado estado;

    /**
     * Clase constructor que permite inicializar los datos de modelo, vista, estado desconectado e inicializar tablas y
     * Listeners
     * @param vista Instancia de la clase Vista
     * @param modelo Instancia de la clase Modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        estado = tipoEstado.desconectado;
        pass = "comecocos";

        iniciarTabla();
        iniciarTabla1();
        addActionListener(this);
        addTableModelListeners(this);
        addWindowListener(this);
    }

    /**
     * Creamos el Listener para las ventanas
     * @param listener instancia de la clase WindowListener
     */
    private void addWindowListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    /**
     * Creamos el Listener para los botones
     * @param listener instancia de la clase ActionListener
     */
    private void addActionListener(ActionListener listener){
        vista.buscarButton.addActionListener(listener);
        vista.eliminarButton.addActionListener(listener);
        vista.nuevoButton.addActionListener(listener);
        vista.verInstructoresButton.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);
        vista.itemCrearTabla2.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemUser.addActionListener(listener);
    }

    /**
     * Creamos el Listener para las tablas
     * @param listener instancia de la clase TableModelListener
     */
    private void addTableModelListeners(TableModelListener listener){
        vista.dtm.addTableModelListener(listener);
        vista.dtm1.addTableModelListener(listener);
    }


    /**
     * Permite detectar los botones de la interfaz y hacer acciones al ejecutarse
     * @param e instancia de la clase ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String accion = e.getActionCommand();

        switch(accion){
            case "Nuevo":
                try {
                    if (vista.clienteRadioButton.isSelected()){
                        if(modelo.comprobarCodigo(vista.txtCodigo.getText())){
                            modelo.nuevoCliente(vista.txtNombre.getText(),vista.txtApellidos.getText(),vista.txtDni.getText(),
                                    Integer.parseInt(vista.txtAltura.getText()), vista.txtCodigo.getText(),vista.dateTimePicker.getDateTimePermissive());
                            cargarFilas(modelo.sacarDatos());
                            vaciarFilas();
                        }else{
                            Util.msgError("No existe ningún instructor con ese código");
                        }
                    }else if(vista.instructorRadioButton.isSelected()){
                        String contra = "";

                        contra = JOptionPane.showInputDialog(null, "Introduce la contraseña", "Contraseña", JOptionPane.QUESTION_MESSAGE);

                        if (contra.equalsIgnoreCase(pass)){
                            modelo.nuevoInstructor(vista.txtNombre.getText(),vista.txtApellidos.getText(),vista.txtDni.getText(),
                                    Integer.parseInt(vista.txtAltura.getText()), vista.txtCodigo.getText(),vista.dateTimePicker.getDateTimePermissive());
                            cargarFilas(modelo.sacarDatos());
                            vaciarFilas();
                        }else{
                            Util.msgError("Contraseña incorrecta");
                        }

                    }else if (!vista.clienteRadioButton.isSelected() && !vista.instructorRadioButton.isSelected()){
                        Util.msgError("Tienes que seleccionar Cliente/Instructor");
                    }else{
                        Util.msgError("Error, comprueba campos");
                    }
                } catch (SQLException e1) {
                    Util.msgError("Comprueba que no hay un DNI duplicado");
                    e1.printStackTrace();
                }
                break;

            case "Buscar":
                try{
                    String dni = "";

                    dni = JOptionPane.showInputDialog(null, "Introduce el dni de un cliente", "Contraseña", JOptionPane.QUESTION_MESSAGE);
                    if ( modelo.buscarCliente(dni) != null){
                        cargarFilas(modelo.buscarCliente(dni));
                    }else{
                        Util.msgError("ERROR, NO EXISTE ESE DNI EN LA BBDD");
                    }
                }catch(SQLException e2){
                    e2.printStackTrace();
                }
                break;
            case "CrearTablaCliente":
                try {
                    modelo.crearTablaClientes();
                    vista.lblAction.setText("Tabla clientes creada");
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "CrearTablaInstructor":
                try {
                    modelo.crearTablaInstructores();
                    vista.lblAction.setText("Tabla instructores creada");
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "Ver Instructores":
                try {
                    cargarFilas1(modelo.clientesSegunCodigo());
                } catch (SQLException ex) {
                    Util.msgError("Error, comprueba que tienes metido el procedimiento");
                    try {
                        cargarFilas1(modelo.sacarDatosCodigo());
                    }catch (SQLException ex1){
                        ex1.printStackTrace();
                    }
                    ex.printStackTrace();
                }
                break;
            case "Eliminar":
                try {
                    int filaBorrar=vista.tablaCliente.getSelectedRow();
                    int idBorrar= (Integer) vista.dtm.getValueAt(filaBorrar,0);
                    modelo.borrarCliente(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    vista.lblAction.setText("Fila Eliminada");

                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Salir":
                System.exit(0);
                break;

            case "UserPass":
                String user = "";
                String pass = "";

                user = JOptionPane.showInputDialog(null, "Introduce el usuario de la bbdd", "Usuario", JOptionPane.QUESTION_MESSAGE);
                pass = JOptionPane.showInputDialog(null, "Introduce la contraseña de la bbdd", "Contraseña", JOptionPane.QUESTION_MESSAGE);

                modelo.nuevaConf(user,pass);
                break;

            case "Conectar":
                if (estado==tipoEstado.desconectado)  {
                    try {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        estado=tipoEstado.conectado;
                        cargarFilas(modelo.sacarDatos());
                    } catch (SQLException e1) {
                        Util.msgError("Error de conexión, comprueba que estén las tablas creadas");
                        e1.printStackTrace();
                    }

                } else {
                    try {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado=tipoEstado.desconectado;
                        vista.dtm.setRowCount(0);
                        vista.lblAction.setText("Desconectado");
                    } catch (SQLException e1) {
                        Util.msgError("Error de conexión, comprueba que esté todo conectado");
                        e1.printStackTrace();
                    }


                }
                break;

        }
    }


    /**
     * Permite vaciar todas las filas y comboBoxe's
     */
    private void vaciarFilas() {
        vista.txtNombre.setText("");
        vista.txtApellidos.setText("");
        vista.txtDni.setText("");
        vista.txtAltura.setText("");
        vista.txtCodigo.setText("");
        vista.dateTimePicker.clear();
        vista.clienteRadioButton.doClick();
        vista.txtNombre.requestFocus();
    }


    /**
     * Permite detectar cuando cambia el estado de las tablas para cambiarlo en la BBDD
     * @param e Instancia de la clase TableModelEvent
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() ==TableModelEvent.UPDATE) {
            int filaModicada=e.getFirstRow();

            try {
                modelo.modificarCliente((Integer)vista.dtm.getValueAt(filaModicada,0),
                        (String)vista.dtm.getValueAt(filaModicada,1),
                        (String)vista.dtm.getValueAt(filaModicada,2),
                        (String)vista.dtm.getValueAt(filaModicada,3),
                        (Integer) vista.dtm.getValueAt(filaModicada,4),
                        (String)vista.dtm.getValueAt(filaModicada,5),
                        (java.sql.Timestamp)vista.dtm.getValueAt(filaModicada,6));
                vista.lblAction.setText("Columna actualizada");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }


    /**
     * Inicializa la primera tabla y le pone los headers
     */
    private void iniciarTabla(){
        String[] headers={"id","nombre","apellidos","dni","altura","Codigo instructor","fecha matriculacion"};
        vista.dtm.setColumnIdentifiers(headers);
    }


    /**
     * Carga las filas en la tabla 1 con los datos de la bbdd (Tabla clientes)
     */
    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[7];
        vista.dtm.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);

            vista.dtm.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAction.setVisible(true);
            vista.lblAction.setText(resultSet.getRow()+" filas cargadas");
        }


    }


    /**
     * Inicializa la segunda tabla y le pone los headers
     */
    private void iniciarTabla1(){
        String[] headers={"Codigo","Cuantos"};
        vista.dtm1.setColumnIdentifiers(headers);
    }

    /**
     * Carga las filas en la tabla 2 con el resultado de la bbdd.
     */
    private void cargarFilas1(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[2];
        vista.dtm1.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);

            vista.dtm1.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAction.setVisible(true);
            vista.lblAction.setText(resultSet.getRow()+" filas cargadas");
        }


    }


    @Override
    public void windowOpened(WindowEvent e) {

    }


    /**
     * Permite comprobar mediante un mensaje de verificación, si de verdad queremos salir de la aplicación.
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.msgConfirmacion("¿Desea salir de la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            System.exit(0);
        }else{
            System.out.println("Pues sigue");
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
