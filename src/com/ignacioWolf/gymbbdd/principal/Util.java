package com.ignacioWolf.gymbbdd.principal;

import javax.swing.*;

public class Util {

    /**
     * Crea un mensaje de error introduciendo el texto por parámetro
     * @param mensaje el texto del error
     */
    public static void msgError(String mensaje) {
        JOptionPane.showMessageDialog(null,mensaje,"ERROR OCURRIDO",JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Crea un mensaje de confirmación (SI/NO) introduciendo el mensaje y titulo por parámetro
     * @param mensaje el mensaje a mostrar
     * @param titulo el titulo de la ventana
     * @return
     */
    public static int msgConfirmacion(String mensaje, String titulo) {
        return JOptionPane.showConfirmDialog(null,mensaje,titulo,JOptionPane.YES_NO_OPTION);
    }
}
