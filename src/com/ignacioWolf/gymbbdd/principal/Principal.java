package com.ignacioWolf.gymbbdd.principal;

import com.ignacioWolf.gymbbdd.gui.Controlador;
import com.ignacioWolf.gymbbdd.gui.Modelo;
import com.ignacioWolf.gymbbdd.gui.Vista;

import java.io.IOException;

/**
 * Trabajo de bases de datos sobre mi tematica Clientes/Instructores, añadiendo el sistema MVC.
 * Mi aportación personal es:
 * - Varias tablas Instructor/Cliente con código de Instructor único y DNI único en los Clientes.
 * - Añadida una contraseña donde solo los instructores con el código correcto podrán registrarse.
 * @author Nacho De Haro / SrWolfMoon
 * @see Controlador, Modelo, Vista
 * @see Util, Principal
 * @version 07/01/2021   1.0
 * @since 1.8
 *
 */

public class Principal {
    public static void main(String[] args) {
        Modelo modelo = null;
        try {
            Vista vista = new Vista();
            modelo = new Modelo();
            Controlador controlador = new Controlador(vista, modelo);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
