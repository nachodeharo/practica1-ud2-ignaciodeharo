DELIMITER //
CREATE PROCEDURE crearTablaClientes()
BEGIN
       CREATE TABLE IF NOT EXISTS clientes(
            id int primary key auto_increment,
            nombre varchar(30),
            apellidos varchar(30),
            dni varchar (9) unique,
            altura int,
            codigoInstructor varchar(30),
            fecha_matriculacion timestamp);
END //
