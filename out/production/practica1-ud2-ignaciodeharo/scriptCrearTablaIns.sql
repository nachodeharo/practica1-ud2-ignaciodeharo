DELIMITER //
CREATE PROCEDURE crearTablaInstructores()
BEGIN
    CREATE TABLE IF NOT EXISTS instructor(
        id int primary key auto_increment,
        nombre varchar(30),
        apellidos varchar(30),
        dni varchar (9) unique,
        altura int,
        numeroIdentificador varchar(30) unique,
        fecha_matriculacion timestamp);
END //
