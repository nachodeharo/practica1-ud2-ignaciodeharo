CREATE DATABASE gimnasio;

use gimnasio;

CREATE TABLE clientes(
id int primary key auto_increment,

nombre varchar(30),

apellidos varchar(30),

dni varchar (9) unique,

altura int,

codigoInstructor varchar(30),

fecha_matriculacion timestamp
);

CREATE TABLE instructor(
id int primary key auto_increment,

nombre varchar(30),

apellidos varchar(30),

dni varchar (9) unique,

altura int,

numeroIdentificador varchar(30) unique,

fecha_matriculacion timestamp
);




INSERT INTO clientes(nombre, apellidos, dni, altura, codigoInstructor, fecha_matriculacion)

VALUES('Ignacio', 'De Haro', '77135722R','174', 'FDW-1', '2020-10-10');


INSERT INTO instructor(nombre, apellidos, dni, altura, numeroIdentificador, fecha_matriculacion)

VALUES('Jorge', 'Gran', '7732722R','182', 'FDW-1', '2020-11-02');
 

select * from gimnasio;